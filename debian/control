Source: alot
Section: mail
Priority: optional
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders:
 Jordan Justen <jordan.l.justen@intel.com>,
 Simon Chopin <chopin.simon@gmail.com>,
 Hugo Lefeuvre <hle@debian.org>,
Build-Depends:
 debhelper (>= 11),
 dh-python,
 procps,
 python (>= 2.7),
 python-configobj,
 python-doc,
 python-gpg,
 python-magic (>= 2:0.4.15),
 python-mock,
 python-notmuch,
 python-setuptools,
 python-sphinx (>= 1.0.7+dfsg),
 python-twisted,
 python-urwid (>= 1.1.0),
 python-urwidtrees (>= 1.0),
Standards-Version: 4.1.4
Homepage: https://github.com/pazz/alot/
Vcs-Git: https://salsa.debian.org/python-team/applications/alot.git
Vcs-Browser: https://salsa.debian.org/python-team/applications/alot

Package: alot
Architecture: all
Depends:
 python-configobj,
 python-gpg,
 python-magic,
 python-notmuch,
 python-twisted,
 python-urwid (>= 1.1.0),
 python-urwidtrees (>= 1.0),
 ${misc:Depends},
 ${python:Depends},
Suggests:
 alot-doc,
Recommends:
 notmuch,
 w3m | links | links2,
Description: Text mode MUA using notmuch mail
 Alot is a text mode mail user agent for the notmuch mail system.
 It features a modular and command prompt driven interface to provide
 a full MUA experience as an alternative to the Emacs and Vim modes shipped
 with notmuch.

Package: alot-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Suggests:
 python-doc,
Description: Text mode MUA using notmuch mail - documentation
 Alot is a terminal-based mail user agent for the notmuch mail system.
 It features a modular and command prompt driven interface to provide
 a full MUA experience as an alternative to the Emacs and Vim modes shipped
 with notmuch.
 .
 This package provides detailed documentation on alot usage.
